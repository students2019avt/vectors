### Задание «Векторы»

Разработать класс `Vector`, задаваемых координатами концов вектора в трехмерном пространстве.

Обеспечить выполнение операций:

* вычисление длины;
* сложения;
* вычитания;
* умножение на число;
* векторное произведение;
* скалярное произведение;
* поворот на угол.

Для реализации консольного интерфейса можно воспользоваться шаблоном из задания «Сотрудники».

Так же требуется реализовать модульные тесты, проверяющие позитивные и, если возможно, негативные сценарии для всех операций.
